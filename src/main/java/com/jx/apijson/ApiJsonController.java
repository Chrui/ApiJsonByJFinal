package com.jx.apijson;

import com.jfinal.core.Controller;
import com.jx.apijson.model.Privacy;
import com.jx.apijson.model.User;
import com.jx.apijson.model.Verify;

import static zuo.biao.apijson.RequestMethod.*;

/**
 * @author jx
 */
public class ApiJsonController extends Controller {
    public static final String USER_;
    public static final String PRIVACY_;
    public static final String VERIFY_;

    static {
        USER_ = User.class.getSimpleName();
        PRIVACY_ = Privacy.class.getSimpleName();
        VERIFY_ = Verify.class.getSimpleName();
    }

    public static final String ID = "id";
    public static final String USER_ID = "userId";


    public void get() {
        renderText(new DemoParser(GET).parse(getRawData()));
    }

    public void head() {
        renderText(new DemoParser(HEAD).setSession(getRequest().getSession()).parse(getRawData()));
    }

    public void gets() {
        renderText(new DemoParser(GETS).setSession(getRequest().getSession()).parse(getRawData()));
    }

    public void heads() {
        renderText(new DemoParser(HEADS).setSession(getRequest().getSession()).parse(getRawData()));
    }

    public void post() {
        renderText(new DemoParser(POST).setSession(getRequest().getSession()).parse(getRawData()));
    }

    public void put() {
        renderText(new DemoParser(PUT).setSession(getRequest().getSession()).parse(getRawData()));
    }

    public void delete() {
        renderText(new DemoParser(DELETE).setSession(getRequest().getSession()).parse(getRawData()));
    }

}
