/*Copyright ©2016 TommyLemon(https://github.com/TommyLemon/APIJSON)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package com.jx.apijson;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.alibaba.fastjson.JSONObject;
import com.jx.apijson.model.*;
import zuo.biao.apijson.MethodAccess;
import zuo.biao.apijson.server.AbstractVerifier;
import zuo.biao.apijson.server.Visitor;

import javax.servlet.http.HttpSession;


/**
 * 权限验证器
 *
 * @author Lemon
 */
public class DemoVerifier extends AbstractVerifier<Long> {
    private static final Log log = LogFactory.get();
    private static final String TAG = "DemoVerifier";


    // <TableName, <METHOD, allowRoles>>
    // <User, <GET, [OWNER, ADMIN]>>
    static { //注册权限
        ACCESS_MAP.put(User.class.getSimpleName(), getAccessMap(User.class.getAnnotation(MethodAccess.class)));
        ACCESS_MAP.put(Privacy.class.getSimpleName(), getAccessMap(Privacy.class.getAnnotation(MethodAccess.class)));
        ACCESS_MAP.put(Moment.class.getSimpleName(), getAccessMap(Moment.class.getAnnotation(MethodAccess.class)));
        ACCESS_MAP.put(Comment.class.getSimpleName(), getAccessMap(Comment.class.getAnnotation(MethodAccess.class)));
        ACCESS_MAP.put(Verify.class.getSimpleName(), getAccessMap(Verify.class.getAnnotation(MethodAccess.class)));
        ACCESS_MAP.put(Login.class.getSimpleName(), getAccessMap(Login.class.getAnnotation(MethodAccess.class)));
    }


    @Override
    public DemoParser createParser() {
        DemoParser parser = new DemoParser();
        parser.setVisitor(visitor);
        return parser;
    }

    @Override
    public String getVisitorKey() {
        return ApiJsonController.USER_;
    }

    @Override
    public String getVisitorIdKey() {
        return ApiJsonController.USER_ID;
    }

    @Override
    public String getVisitorIdKey(String table) {
        return ApiJsonController.USER_.equals(table) || ApiJsonController.PRIVACY_.equals(table) ? ApiJsonController.ID : getVisitorIdKey();
    }

    /**
     * 登录校验
     *
     * @param session
     * @throws Exception
     * @author
     * @modifier Lemon
     */
    public static void verifyLogin(HttpSession session) throws Exception {
        log.debug(TAG, "verifyLogin  session.getId() = " + (session == null ? null : session.getId()));
        new DemoVerifier().setVisitor(getVisitor(session)).verifyLogin();
    }


    /**
     * 获取来访用户的id
     *
     * @param session
     * @return
     * @author Lemon
     */
    public static long getVisitorId(HttpSession session) {
        if (session == null) {
            return 0;
        }
        Long id = (Long) session.getAttribute(ApiJsonController.USER_ID);
        if (id == null) {
            Visitor<Long> v = getVisitor(session);
            id = v == null ? 0 : value(v.getId());
            session.setAttribute(ApiJsonController.USER_ID, id);
        }
        return value(id);
    }

    /**
     * 获取来访用户
     *
     * @param session
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Visitor<Long> getVisitor(HttpSession session) {
        return session == null ? null : (Visitor<Long>) session.getAttribute(ApiJsonController.USER_);
    }

    public static long value(Long v) {
        return v == null ? 0 : v;
    }

    /**
     * 删除请求里的权限信息
     *
     * @param requestObject
     * @return
     */
    @Override
    @Deprecated
    public JSONObject removeAccessInfo(JSONObject requestObject) {
        return requestObject;
    }

}
