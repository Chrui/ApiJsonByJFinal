import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jx.apijson.DemoParser;
import org.junit.Test;

import static zuo.biao.apijson.RequestMethod.GET;

public class GetTest {
    private Prop p;

    public void db() {
        p = PropKit.use("config.properties");
        DruidPlugin dp = new DruidPlugin(p.get("url"), p.get("user"), p.get("password"));
        ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
        dp.start();
        arp.start();
    }

    @Test
    public void test1() {
        db();
        String json = "{'User':{'id': 82001},'[]': {'count':3,'Comment': {'userId@': 'User/id'}}}";
        //System.out.println(Db.find("select * from user"));
        String res = new DemoParser(GET).parse(json);
        System.out.println(res);
    }
}
